package com.project.employeeservice.constant;

public enum Gender {
    MALE,
    FEMALE
}
