package com.project.employeeservice.controller;

import com.project.employeeservice.model.EmployeeDetails;
import com.project.employeeservice.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/employee")
public class EmployeeController {

    @Autowired
    EmployeeService employeeService;

    @GetMapping("/getEmployeesList")
    public List<EmployeeDetails> getAllUsers() {
        return employeeService.findAllEmployees();
    }

    @RequestMapping(value="/register",method=RequestMethod.POST)
    public EmployeeDetails createEmployee(@Valid @RequestBody EmployeeDetails employeeDetails) {
        return employeeService.saveEmployeeDetail(employeeDetails);
    }
}
