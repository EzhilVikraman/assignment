package com.project.employeeservice.repository;

import com.project.employeeservice.model.EmployeeDetails;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface EmployeeRepository extends JpaRepository<EmployeeDetails, Long> {

    @Override
    List<EmployeeDetails> findAll();

}
