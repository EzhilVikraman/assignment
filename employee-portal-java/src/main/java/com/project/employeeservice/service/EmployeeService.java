package com.project.employeeservice.service;

import com.project.employeeservice.model.EmployeeDetails;
import com.project.employeeservice.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class EmployeeService {

    @Autowired
    EmployeeRepository employeeRepository;

    @Transactional
    public List<EmployeeDetails> findAllEmployees() {
        return employeeRepository.findAll();
    }

    @Transactional
    public EmployeeDetails saveEmployeeDetail(EmployeeDetails employeeDetails) {
        return employeeRepository.save(employeeDetails);
    }
}
