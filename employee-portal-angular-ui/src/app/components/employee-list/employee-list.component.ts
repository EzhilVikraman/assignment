import { Component, OnInit } from '@angular/core';
import { Employee } from 'src/app/model/employee';
import { EmployeeServiceService } from 'src/app/service/employee-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css']
})
export class EmployeeListComponent implements OnInit {

  employeesList: Employee[];

  constructor(
    private employeeService: EmployeeServiceService,
    private router: Router,


  ) { }

  ngOnInit() {
    this.getEmployeessList();
  }

  createEmployee() {
    this.router.navigate(['/employee']);
  }

  getEmployeessList(): void {
      this.employeeService.getEmployeeList()
      .subscribe((response) => {
        if (response) {
          this.employeesList = response 
        }
      },
      (error) => {
        console.error()
      });
    
  }
}
