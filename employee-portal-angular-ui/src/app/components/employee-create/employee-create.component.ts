import { Component, OnInit } from '@angular/core';
import { EmployeeServiceService } from 'src/app/service/employee-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-employee-create',
  templateUrl: './employee-create.component.html',
  styleUrls: ['./employee-create.component.css']
})
export class EmployeeCreateComponent implements OnInit {

  response: any;
  formValue: {};
  constructor(
    private employeeService: EmployeeServiceService,
    private router: Router,
  ) { }

  ngOnInit() {
  }

  onSubmit(formValue) {
    let formData = formValue.value;
    let payLoad = this.formatPayLoad(formData);
    this.employeeService.saveEmployee(payLoad).subscribe(
      response => {
        this.response = response;
      });
  }

  private formatPayLoad(formData: any) {
    let payLoad = {};
    
    payLoad = {
      firstName: formData.firstName,
      lastName: formData.lastName,
      gender: formData.gender,
      department: {
        depName: formData.department
      },
      dateOfBirth: new Date(formData.dateOfBirth)
    };
    return payLoad;
  }

  navigateTOEmployee() {
    this.router.navigate(['/list']);
  }

}
