import { BaseDto } from './base-dto';

export class Department extends BaseDto{
    depName: string;
}
