import { Department } from './department';
import { BaseDto } from './base-dto';

export class Employee {
        id:any;
        firstName: string;
        lastName: string;
        gender: string;
        dateOfBirth: Date
        department: Department
    
        public constructor(
            fields?: {
                id?: any;
                firstName?: string;
                lastName?: string;
                gender?: string;
                dateOfBirth?: Date
                department?: Department
            }) { if (fields) { Object.assign(this, fields); } }
    
}
