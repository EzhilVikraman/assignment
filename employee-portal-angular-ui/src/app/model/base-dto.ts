export class BaseDto {
    id?: string;
    name?: string;
    entities?: any;

    public constructor(
        fields?: {
            id?: string,
            name?: string,
            entities?: any
        }) { if (fields) { Object.assign(this, fields); } }
}
