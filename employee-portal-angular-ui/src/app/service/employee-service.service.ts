import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { from, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { BaseDto } from '../model/base-dto';

@Injectable({
  providedIn: 'root'
})
export class EmployeeServiceService {

  constructor(private http: HttpClient) {
  }

  saveEmployee(payload): Observable<any> {
    return this.http.post(environment.BACKEND_URL + "/api/employee/register", payload);
  }

  getEmployeeList(): Observable<any> {
    return this.http.get<any>(environment.BACKEND_URL + "/api/employee/getEmployeesList").pipe();
  }
}
